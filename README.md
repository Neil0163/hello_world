Week 1 of the specalist course for DevOps 

1) Create relevant project directory, files and folders for application such as lib, seeds, templates and test folder: 
Start to create these and fill them out as per the software engineering phase (dosent matter for this specific week)
2) Build your App.py file
3) Create a new project on GitLab and set up remote ( commands are the same as GitHub, set up SSH refer to AI if you need help with this)

!This week's focus content! 
*** Containerising the App ***
1) Run Docker init - This will prompt you with specific questions on your project and it will create your docker files.
2) Run Docker, if Docker isnt working in the background your builds will not build
3) Run Docker compose up --build This will then take the build content from the automated docker file and build the image. Docker compose   will then start the container as well as handling the orchestration and networking. Note that if you make changes to any routes in the app you will have to stop the container and then rebuild and deploy it again so those changes come into effect.
4) Running in detched mode, docker compose up --build -d this runs in detached mode, meaning that the container will not be connected to the terminal, its run in the background which allows the container to run even when the shell or the terminal is closed. It helps when running automation task as part of a script as they require minimal intervention or monitoring. Different componnents of an app my be required to run  in seperate containers at seperate times. This is what this mode is also used for. 
5) Docker compose down - By running this command you stop the container that is currentley running. 


*** Automatically updating Services ***
1) Use Compose Watch to automatically update your running Compose services as you edit and save your code. 
2) Run compose watch by typing docker compose watch 


